import React from 'react';

import {
    Container,
    Box,
    Button
} from '@material-ui/core';

const Menu = ({ history }) => {
    return (
        <Container>
            <Box display="flex" p={2} justifyContent="center" marginTop={30}>
                <Button variant="contained" color="primary" onClick={e => { history.push('/show1') }}>SHOW 1</Button>
            </Box>
            <Box display="flex" p={2} justifyContent="center">
                <Button variant="contained" color="primary" onClick={e => { history.push('/show2') }}>SHOW 2</Button>
            </Box>
            <Box display="flex" p={2} justifyContent="center">
                <Button variant="contained" color="primary" onClick={e => { history.push('/show3') }}>SHOW 3</Button>
            </Box>
        </Container>
    );
}

export default Menu;