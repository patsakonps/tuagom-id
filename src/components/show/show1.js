import React, { useState } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
  Grid,
  IconButton,
  InputBase,
  Chip,
  CircularProgress,
  Box,
  Typography,
} from '@material-ui/core';
// import {
//   Search as SearchIcon,
//   DoubleArrow as DoubleArrowIcon,
//   CheckCircle as CheckCircleIcon
// } from '@material-ui/icons';
import { Link, Redirect } from 'react-router-dom';

import { escape, escapeRegExp } from 'lodash-es';
import 'antd/dist/antd.css';
import { Carousel } from 'antd';

import {
  joinClasses,
  LightenDarkenColor,
  delay,
  extractErrorRespMsg,
} from 'utils/utility';

import { useImagesContext } from '../../contexts/ImagesContext';
import { setSelectedBranch } from '../../actions/branch';
import { handleError } from '../../services/handleResponse';
import { logout } from '../../actions/auth';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '2160px',
    height: '3840px',
    // width: '100vw',
    // height: '100vh',
    // width: 'fit-content',
    // height: '100vh',
    overflow: 'hidden',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
  },
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    //width: '2160px',
    height: '100%',
    transform: 'translateY(120px)',
  },
  item: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '40px',
    '& [text-label]': {
      position: 'absolute',
      left: 136,
      textTransform: 'uppercase',
      fontSize: '5.5rem',

      textShadow: '1px 3px 3px #000000',
      //color: '#fff',
      fontWeight: 700,
      '-webkit-text-stroke': '1px #fff',
    },
    '& [text-student]': {
      position: 'absolute',
      textTransform: 'uppercase',
      fontSize: '3.8rem',

      textShadow: '1px 2px 2px #000000',
      //color: '#fff',
      fontWeight: 700,
      '-webkit-text-stroke': '.6px #fff',
      bottom: 14,
      zIndex: 100,
      //transform: 'translateX(-224px)',
    },
    '& [single-text-student]': {
      position: 'absolute',
      textTransform: 'uppercase',
      fontSize: '11.4rem',
      fontWeight: 800,
      '-webkit-text-stroke': '1px #fff',
      bottom: 174,
      zIndex: 100,
      //transform: 'translateX(-224px)',
    },
    '& [single-text-level]': {
      position: 'absolute',
      textTransform: 'uppercase',
      fontSize: '8.4rem',
      fontWeight: 800,
      '-webkit-text-stroke': '1px #fff',
      bottom: 49,
      zIndex: 100,
      //transform: 'translateX(-224px)',
    },
    '& [avatar]': {
      position: 'absolute',
      borderRadius: '50%',
      minWidth: '438px',
      minHeight: '438px',
      width: '438px',
      height: '438px',
    },
    '& [single-avatar]': {
      position: 'absolute',
      borderRadius: '50%',
      width: 965,
      height: 965,
    },
  },
  label: {
    width: 'fit-content',
    position: 'absolute',
    zIndex: -1,
    left: 0,
  },
  framePic: {
    width: 'fit-content',
  },
  framePicSingle: {
    width: 'fit-content',
  },
}));

const Show1 = (props) => {
  const { data, mode, ckey } = props;
  const { images } = useImagesContext();
  const c = joinClasses(useStyles({ ...props }));
  //console.log('data:', data);

  return (
    <div
      key={ckey}
      className={c('root')}
      style={{
        backgroundImage: `url(${images['present-background.jpg']})`,
      }}
    >
      <div className={c('container')}>
        {mode === 'multiple' ? (
          <Grid
            container
            spacing={1}
            style={{ minHeight: 1994, minWidth: 2160 }}
          >
            {(data || []).map((row, idx) => (
              <>
                <Grid
                  style={{ minHeight: 458, minWidth: 542 }}
                  key={'label' + idx}
                  item
                  xs={3}
                  className={c('item')}
                >
                  <img
                    className={c('label')}
                    src={images['present-label.png']}
                  />
                  <Typography text-label=''>{row.levelName}</Typography>
                </Grid>
                {(row.students || []).map((student, sidx) => (
                  <Grid
                    key={'label' + idx + 'student' + sidx}
                    item
                    xs={3}
                    className={c('item')}
                    style={{ justifyContent: 'flex-start' }}
                    style={{ minHeight: 458, minWidth: 542 }}
                  >
                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                      <img
                        className={c('framePic')}
                        src={images['present-pic-frame.png']}
                        //style={{ position: 'absolute', left: 0 }}
                      />
                      <Typography text-student=''>{student.name}</Typography>
                      <img
                        avatar=''
                        src={student?.avatar || images['unnamed.png']}
                        style={{ alignSelf: 'flex-start' }}
                      />
                    </div>
                  </Grid>
                ))}
              </>
            ))}
          </Grid>
        ) : mode === 'single' ? (
          <Grid container spacing={1} justify='center' alignItems='center'>
            <Grid item xs={12} className={c('item')}>
              <img
                className={c('framePicSingle')}
                src={images['present-single-pic-frame.png']}
              />

              <img
                single-avatar=''
                src={data?.image || images['unnamed.png']}
              />
            </Grid>
            <Grid item xs={12} className={c('item')}>
              <img
                className={c('framePicSingle')}
                src={images['present-paint.png']}
              />
              <Typography single-text-student=''>{data.name}</Typography>
            </Grid>
            <Grid item xs={12} className={c('item')}>
              <img
                className={c('framePicSingle')}
                src={images['present-under-level.png']}
              />
              <Typography single-text-level=''>
                {'LEVEL ' + data.levelName}
              </Typography>
            </Grid>
          </Grid>
        ) : null}
      </div>
    </div>
  );
};

const mapStateToProps = ({ config, auth }) => ({
  auth,
  config,
});

const mapDispatchToProps = { setSelectedBranch, logout };

export default connect(mapStateToProps, mapDispatchToProps)(Show1);
