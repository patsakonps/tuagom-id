import React, { useState } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
  Grid,
  IconButton,
  InputBase,
  Chip,
  CircularProgress,
  Box,
  Typography,
} from '@material-ui/core';
// import {
//   Search as SearchIcon,
//   DoubleArrow as DoubleArrowIcon,
//   CheckCircle as CheckCircleIcon
// } from '@material-ui/icons';
import { Link, Redirect } from 'react-router-dom';

import { escape, escapeRegExp } from 'lodash-es';
import 'antd/dist/antd.css';
import { Carousel } from 'antd';

import {
  joinClasses,
  LightenDarkenColor,
  delay,
  extractErrorRespMsg,
} from 'utils/utility';

import {
  getShow1 as getShow1Service,
  getStudentShowDetail as getStudentShowDetailService,
} from '../../services/show';
import { useImagesContext } from '../../contexts/ImagesContext';
import { setSelectedBranch } from '../../actions/branch';
import { handleError } from '../../services/handleResponse';
import { logout } from '../../actions/auth';
import Show1 from './show1';
import { ResetIcon } from 'evergreen-ui';
import imgLoading from 'images/circle-loading-animation1.gif';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '2160px',
    height: '3840px',
    // width: '100vw',
    // height: '100vh',
    // width: 'fit-content',
    // height: '100vh',
    overflow: 'hidden',
    // backgroundRepeat: 'no-repeat',
    // backgroundPosition: 'center',
    // backgroundSize: 'contain',
  },
  //   container: {
  //     display: 'flex',
  //     justifyContent: 'center',
  //     alignItems: 'center',
  //     width: 1212,
  //     height: '100%',
  //     transform: 'translateY(120px)',
  //   },
}));

const Show = (props) => {
  // const { images } = useImagesContext();
  const c = joinClasses(useStyles({ ...props }));
  const [showData, setShowData] = useState([]);
  const [showDataReqFlag, setShowDataReqFlag] = useState(false);
  const [showDataSuccessFlag, setShowDataSuccessFlag] = useState(false);
  const [autoplaySpeed, setAutoplaySpeed] = useState(15000);

  const [studentShowDetail, setStudentShowDetail] = useState([]);
  const [
    getStudentShowDetailReqFlag,
    setGetStudentShowDetailReqFlag,
  ] = useState(false);
  const [
    getStudentShowDetailSuccessFlag,
    setGetStudentShowDetailSuccessFlag,
  ] = useState(false);
  React.useEffect(() => {
    getShowData();
    getStudentShowDetail();
  }, []);

  const getShowData = () => {
    setShowDataReqFlag(true);
    setShowDataSuccessFlag(false);
    getShow1Service(
      (resp) => {
        setShowData(resp);
        setShowDataReqFlag(false);
        setShowDataSuccessFlag(true);
      },
      (error) => {
        handleError(error);
        setShowDataReqFlag(false);
      }
    );
  };

  const getStudentShowDetail = () => {
    setGetStudentShowDetailReqFlag(true);
    setGetStudentShowDetailSuccessFlag(false);
    getStudentShowDetailService(
      (resp) => {
        setStudentShowDetail(resp || []);
        setGetStudentShowDetailReqFlag(false);
        setGetStudentShowDetailSuccessFlag(true);
      },
      (error) => {
        handleError(error);
        setGetStudentShowDetailReqFlag(false);
      }
    );
  };
  //   const [showData, setShowData] = useState([]);

  //   React.useEffect(() => {
  //     getShow1Service(
  //       (resp) => {
  //         setShowData(resp);
  //       },
  //       (error) => {
  //         handleError(error);
  //       }
  //     );
  //   }, []);

  // const get12FristStudent = (dataList) => {
  //   // return (dataList || [])
  //   //   .reduce((accum, level) => {
  //   //     return [...accum, ...level.students];
  //   //   }, [])
  //   //   .slice(0, 12);

  // };

  const handleCarouselafterChange = (current) => {
    console.log('handleCarouselafterChange:', current);
    if (current === 0) {
      //reset();
    } else if (current === 2) {
      setAutoplaySpeed(5000);
    }
  };

  const handleCarouselbeforeChange = (current, next) => {
    console.log('handleCarouselbeforeChange:', next);
    if (next === 0) {
      reset();
    }
  };

  const reset = () => {
    setShowData([]);
    setAutoplaySpeed(15000);
    getShowData();
    getStudentShowDetail();
  };
  //console.log('showData:', get12FristStudent(showData));
  return (
    <div className={c('root')}>
      {!showDataReqFlag &&
      showDataSuccessFlag &&
      (showData || []).length &&
      !getStudentShowDetailReqFlag &&
      getStudentShowDetailSuccessFlag ? (
        <Carousel
          autoplay
          autoplaySpeed={autoplaySpeed}
          style={{ width: '2160px', height: '3840px' }}
          afterChange={handleCarouselafterChange}
          beforeChange={handleCarouselbeforeChange}
          pauseOnHover={false}
        >
          <div key={'s_1'}>
            <Show1
              ckey={'ss_1'}
              mode={'multiple'}
              data={showData.slice(0, 4)}
            />
          </div>
          <div key={'s_2'}>
            <Show1
              ckey={'ss_2'}
              mode={'multiple'}
              data={showData.slice(4, 8)}
            />
          </div>
          {studentShowDetail.map((student, idx) => (
            <div key={'ss_' + idx}>
              <Show1 ckey={'sss_' + idx} mode={'single'} data={student} />
            </div>
          ))}
        </Carousel>
      ) : (
        <img src={imgLoading} />
      )}
    </div>
  );
};

const mapStateToProps = ({ config, auth }) => ({
  auth,
  config,
});

const mapDispatchToProps = { setSelectedBranch, logout };

export default connect(mapStateToProps, mapDispatchToProps)(Show);
