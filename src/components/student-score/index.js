import React, { useState } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
// import {
//   Search as SearchIcon,
//   DoubleArrow as DoubleArrowIcon,
//   CheckCircle as CheckCircleIcon
// } from '@material-ui/icons';
import { joinClasses, extractErrorRespMsg } from 'utils/utility';
import { clickAnime } from 'utils/element';
import { login } from '../../services/auth';
import { useImagesContext } from '../../contexts/ImagesContext';
import { useDispatch, useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    display: 'flex',
    flexFlow: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100vw',
    height: '100vh',
    overflow: 'auto',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },

  'img-top-bar': {
    position: 'absolute',
    width: '100%',
    height: '105%',
    maxHeight: '5rem',
    zIndex: 1,
  },
  'img-left-bar': {
    position: 'absolute',
    top: 0,
    left: '24%',
    zIndex: 1,
    width: '2.5%',
    maxWidth: '1.4rem',
    height: '100%',
    //height: '76vh',
    transform: 'translateY(-.3rem)',
  },
  'img-right-bar': {
    position: 'absolute',
    top: 0,
    right: '0%',
    zIndex: 0,
    width: '2.5%',
    maxWidth: '1.4rem',
    height: '100%',
    //height: '76vh',
    transform: 'translateY(-.3rem)',
  },
  'img-bottom-bar': {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: '100%',
    maxHeight: '3.8rem',
    zIndex: 1,
    //transform: 'translateY(-24vh)',
  },
  'img-stick': {
    height: 'max-content',
    maxHeight: '15%',
    width: 'calc(100% + .5rem)',
    alignSelf: 'flex-end',
    transform: 'translate(.1rem, .4rem)',
    zIndex: 2,
  },
  'img-frame-main': {
    width: '98%',
    maxWidth: '7rem',
  },
  'img-left': {
    width: '100%',
    maxWidth: '3.5rem',
    height: '100%',
  },
  'img-right': {
    width: '100%',
    maxWidth: '3.5rem',
    height: '100%',
  },
  'img-up': {
    position: 'absolute',
    right: '-1.5%',
    top: 'calc(-9.5%)',
    width: '14%',
    height: '14%',
    maxWidth: '3rem',
    maxHeight: '3rem',
    zIndex: 3,
  },
  'img-down': {
    position: 'absolute',
    right: '-1.5%',
    bottom: 'calc(-9.5%)',
    width: '14%',
    height: '14%',
    maxWidth: '3rem',
    maxHeight: '3rem',
    zIndex: 3,
  },
  'img-win01': {
    width: '50%',
    maxWidth: '5rem',
    height: '20%',
    paddingBottom: '.2rem',
  },
  'img-winbox': {
    width: '75%',
    maxWidth: '7rem',
    height: '80%',
  },
  'img-lose01': {
    width: '50%',
    maxWidth: '5rem',
    height: '20%',
    paddingBottom: '.2rem',
  },
  'img-losebox': {
    width: '75%',
    maxWidth: '7rem',
    height: '80%',
  },
  'img-score': {
    width: '90%',
    height: '95%',
    maxWidth: '7rem',
    paddingTop: '.2rem',
  },
  'img-back': {
    width: '84%',
    height: '70%',
    maxWidth: '7rem',
  },
  'text-group-name': {
    textAlign: 'center',
    color: '#000',
    textTransform: 'uppercase',
    fontSize: '140%',
    textShadow: '.01rem .01rem .01rem #000000',
    fontWeight: 'bolder',
    '-webkit-text-stroke': '.018rem #fff',
    zIndex: 1,
  },
  'text-round': {
    textAlign: 'center',
    color: '#000',
    textTransform: 'uppercase',
    fontSize: '140%',
    textShadow: '.01rem .01rem .01rem #000000',
    fontWeight: 'bolder',
    '-webkit-text-stroke': '.018rem #fff',
    zIndex: 1,
    height: 'max-content',
  },
  'img-left01': {
    zIndex: 1,
    height: '100%',
  },
  'img-right01': {
    zIndex: 1,
    height: '100%',
  },
  'img-ver-line': {
    height: '105%',
    width: '1.2%',
    maxWidth: '.3rem',
    position: 'absolute',
    zIndex: 1,
    transform: 'translateY(-2%)',
  },
  'img-hor-line': {
    width: '100%',
    height: '2.6%',
    position: 'absolute',
    zIndex: 0,
  },
  'img-teacher': {
    width: '90%',
    height: '90%',
    maxWidth: '6rem',
  },
  'img-enemy': {
    width: '100%',
    height: '100%',
  },
  'box-round': {
    height: '20%',
    width: '100%',
  },
  'text-enemy-name': {
    textAlign: 'center',
    color: '#fff',
    fontSize: '110%',
    textShadow: '.01rem .01rem .01rem #000000',
    fontWeight: '800',
    '-webkit-text-stroke': '.03rem #000',
    zIndex: 1,
    height: 'max-content',
  },
  'text-main-name': {
    textAlign: 'center',
    color: '#fff',
    fontSize: '110%',
    textShadow: '.01rem .01rem .01rem #000000',
    fontWeight: '800',
    '-webkit-text-stroke': '.03rem #000',
    zIndex: 1,
    height: 'max-content',
    transform: 'scale(1.18)',
  },
  'img-mission': {
    width: '40%',
    maxWidth: '12rem',
    height: '58%',
  },
  'img-score-p': {
    width: '80%',
    maxWidth: '10rem',
    height: '65%',
  },
  'img-score-win': {
    width: '80%',
    maxWidth: '9rem',
    height: '35%',
  },
}));

const Login = (props) => {
  const { images } = useImagesContext();
  const c = joinClasses(useStyles({ ...props }));

  const [loginReqFlag, setLoginReqFlag] = useState(false);
  const [loginSuccessFlag, setLoginSuccessFlag] = useState(false);
  const [loginErrorFlag, setLoginErrorFlag] = useState(false);
  const [scoreTeacher, setScoreTeacher] = useState([1, 2, 3, 4, 5]);
  const [scoreEnemy, setScoreEnemy] = useState([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
  const [enemyPageIdx, setEnemyPageIdx] = useState(0);
  const handleClickLogin = () => {};

  const loginService = (user, next = null, error = null) => {
    setLoginReqFlag(true);
    setLoginSuccessFlag(false);
    setLoginErrorFlag(false);
    login(
      user,
      (resp) => {
        setLoginSuccessFlag(true);
        next && next(resp);
      },
      (err) => {
        setLoginErrorFlag(true);
        error && error(err);
      },
      () => {
        setLoginReqFlag(false);
      }
    );
  };

  return (
    <div
      className={c('root')}
      style={{
        backgroundImage: `url(${images['student-score-background.jpg']})`,
      }}
    >
      <div
        style={{
          display: 'flex',
          position: 'relative',
          width: '100%',
          height: '75%',
          flexFlow: 'column',
          alignItems: 'center',
        }}
      >
        <div
          style={{
            width: '100%',
            height: '9%',
            display: 'flex',
            position: 'relative',
            zIndex: 2,
          }}
        >
          <img
            className={c('img-top-bar')}
            src={images['student-score-top.png']}
          />
          <div
            style={{
              display: 'flex',
              flexFlow: 'column',
              width: '24%',
              height: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Typography className={c('text-group-name')}>ชื่อกลุ่ม</Typography>
          </div>
          <div
            style={{
              display: 'flex',

              width: '76%',
              height: '100%',
              //paddingLeft: '2.5%',
              justifyContent: 'space-around',
              alignItems: 'center',
              overflow: 'hidden',
            }}
          >
            <img
              className={c('img-left01')}
              src={images['student-score-left01.png']}
            />
            <Typography className={c('text-round')}>Round 1</Typography>
            <img
              className={c('img-right01')}
              src={images['student-score-right01.png']}
            />
          </div>
        </div>
        <img
          className={c('img-left-bar')}
          src={images['student-score-side.png']}
        />
        <div style={{ display: 'flex', width: '100%', height: '84%' }}>
          <div
            style={{
              display: 'flex',
              flexFlow: 'column',
              width: '24%',
              height: '100%',
              alignItems: 'center',
            }}
          >
            <div
              style={{
                display: 'flex',
                width: '100%',
                height: '20%',
                justifyContent: 'center',
                position: 'relative',
              }}
            >
              <img
                className={c('img-frame-main')}
                src={images['student-score-mainpic.png']}
              />
              <div
                style={{
                  position: 'absolute',
                  bottom: '-6%',
                  width: '100%',
                  display: 'flex',
                  justifyContent: 'center',
                }}
              >
                <Typography className={c('text-main-name')}>ปลื้ม</Typography>
              </div>
            </div>
            <div
              style={{
                display: 'flex',
                width: '100%',
                height: '10%',
                justifyContent: 'space-between',
                alignItems: 'center',
                marginBottom: '.2rem',
              }}
            >
              <div
                style={{
                  width: '40%',
                  height: '80%',
                  display: 'flex',
                  justifyContent: 'flex-end',
                  paddingLeft: '1%',
                }}
              >
                <img
                  className={c('img-left')}
                  src={images['student-score-left.png']}
                />
              </div>
              <div
                style={{
                  width: '40%',
                  height: '80%',
                  display: 'flex',
                  justifyContent: 'flex-start',
                  paddingRight: '1%',
                }}
              >
                <img
                  className={c('img-right')}
                  src={images['student-score-right.png']}
                />
              </div>
            </div>

            <div
              style={{
                display: 'flex',
                flexFlow: 'column',
                width: '100%',
                height: '18%',
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: '.4rem',
              }}
            >
              <img
                className={c('img-win01')}
                src={images['student-score-win01.png']}
              />
              <img
                className={c('img-winbox')}
                src={images['student-score-winbox.png']}
              />
            </div>

            <div
              style={{
                display: 'flex',
                flexFlow: 'column',
                width: '100%',
                height: '18%',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <img
                className={c('img-lose01')}
                src={images['student-score-lose01.png']}
              />
              <img
                className={c('img-losebox')}
                src={images['student-score-losebox.png']}
              />
            </div>

            <div
              style={{
                display: 'flex',
                flexFlow: 'column',
                width: '100%',
                height: '20%',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <img
                className={c('img-score')}
                src={images['student-score-score.png']}
              />
            </div>
            <div
              style={{
                display: 'flex',
                width: '100%',
                height: '14%',
                justifyContent: 'center',
                alignItems: 'flex-start',
              }}
            >
              <img
                className={c('img-back')}
                src={images['student-score-back.png']}
              />
            </div>
          </div>
          <div
            style={{ display: 'flex', width: '2.5%', maxWidth: '1.4rem' }}
          ></div>
          <div
            style={{
              display: 'flex',
              flexFlow: 'column',
              width: '73.5%',
              height: '100%',
              // paddingLeft: '2.5%',
              position: 'relative',
            }}
          >
            <img
              style={{ left: '33.33%' }}
              className={c('img-ver-line')}
              src={images['student-score-ver-line.png']}
            />
            <img
              style={{ left: '66.66%' }}
              className={c('img-ver-line')}
              src={images['student-score-ver-line.png']}
            />

            <div
              style={{
                display: 'flex',
                flexFlow: 'column',
                width: '100%',
                height: '35%',
                position: 'relative',
                overflow: 'hidden',
              }}
            >
              <div
                style={{
                  display: 'flex',
                  flexFlow: 'column',
                  width: '100%',
                  height: '85%',
                  position: 'relative',
                }}
              >
                {Array.from(
                  Array(Math.ceil((scoreTeacher || []).length / 3)).keys()
                ).map((row, rowIdx) => {
                  return (
                    <div
                      style={{ display: 'flex', height: '50%', width: '100%' }}
                    >
                      {(scoreTeacher || [])
                        .slice(rowIdx, rowIdx + 3)
                        .map((col, colIdx) => (
                          <div
                            style={{
                              display: 'flex',
                              justifyContent: 'center',
                              alignItems: 'center',
                              height: '100%',
                              width: '33.33%',
                            }}
                          >
                            <img
                              className={c('img-teacher')}
                              src={images['student-score-teacher.png']}
                            />
                          </div>
                        ))}
                    </div>
                  );
                })}
                <img
                  style={{ top: 'calc(50% - 1.3%)' }}
                  className={c('img-hor-line')}
                  src={images['student-score-hor-line.png']}
                />
                <img
                  style={{ top: 'calc(100%)', transform: 'translateY(.1rem)' }}
                  className={c('img-hor-line')}
                  src={images['student-score-hor-line.png']}
                />
              </div>
              <img
                className={c('img-stick')}
                src={images['student-score-stick.png']}
              />
            </div>
            <div
              style={{
                display: 'flex',
                flexFlow: 'column',
                width: '100%',
                height: '65%',
                position: 'relative',
              }}
            >
              {Array.from(
                Array(
                  Math.ceil(
                    (scoreEnemy || []).slice(
                      enemyPageIdx * 9,
                      enemyPageIdx * 9 + 9
                    ).length / 3
                  )
                ).keys()
              ).map((row, rowIdx) => {
                return (
                  <div
                    style={{ display: 'flex', height: '33.33%', width: '100%' }}
                  >
                    {(scoreEnemy || [])
                      .slice(rowIdx, rowIdx + 3)
                      .map((col, colIdx) => (
                        <div
                          style={{
                            display: 'flex',
                            flexFlow: 'column',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            height: '100%',
                            width: '33.33%',
                          }}
                        >
                          <img
                            className={c('box-round')}
                            src={images['student-score-box-round.png']}
                          />
                          <div
                            style={{
                              width: '90%',
                              height: '68%',
                              maxWidth: '6rem',
                              marginBottom: '.25rem',
                              position: 'relative',
                            }}
                          >
                            <img
                              className={c('img-enemy')}
                              src={images['student-score-enemy.png']}
                            />
                            <div
                              style={{
                                position: 'absolute',
                                bottom: '-6%',
                                width: '100%',
                                display: 'flex',
                                justifyContent: 'center',
                              }}
                            >
                              <Typography className={c('text-enemy-name')}>
                                ปลื้ม
                              </Typography>
                            </div>
                          </div>
                        </div>
                      ))}
                  </div>
                );
              })}
              <img
                className={c('img-up')}
                src={images['student-score-up.png']}
              />
              <img
                className={c('img-down')}
                src={images['student-score-down.png']}
              />
            </div>
          </div>
          <div
            style={{ display: 'flex', width: '2.5%', maxWidth: '1.4rem' }}
          ></div>
        </div>

        <img
          className={c('img-right-bar')}
          src={images['student-score-side.png']}
        />
        <div
          style={{
            width: '100%',
            height: '7%',
            zIndex: 2,
            position: 'relative',
          }}
        >
          <img
            className={c('img-bottom-bar')}
            src={images['student-score-bottom.png']}
          />
        </div>
      </div>
      <div
        style={{
          display: 'flex',
          position: 'relative',
          width: '100%',
          height: '25%',
          flexFlow: 'column',
          alignItems: 'center',
        }}
      >
        <div
          style={{
            width: '92%',
            height: '95%',
            backgroundImage: `url(${images['student-score-mission.png']})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            borderRadius: 20,
          }}
        >
          <div
            style={{
              width: '100%',
              height: '30%',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <img
              className={c('img-mission')}
              src={images['student-score-mission01.png']}
            />
          </div>
          <div
            style={{
              width: '100%',
              height: '70%',
              display: 'flex',
            }}
          >
            <div
              style={{
                width: '33.33%',
                height: '100%',
                display: 'flex',
                flexFlow: 'column',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <img
                className={c('img-score-p')}
                src={images['student-score-20p.png']}
              />
              <img
                className={c('img-score-win')}
                src={images['student-score-win5.png']}
              />
            </div>
            <div
              style={{
                width: '33.33%',
                height: '100%',
                display: 'flex',
                flexFlow: 'column',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <img
                className={c('img-score-p')}
                src={images['student-score-50p.png']}
              />
              <img
                className={c('img-score-win')}
                src={images['student-score-winteacher.png']}
              />
            </div>
            <div
              style={{
                width: '33.33%',
                height: '100%',
                display: 'flex',
                flexFlow: 'column',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <img
                className={c('img-score-p')}
                src={images['student-score-100p.png']}
              />
              <img
                className={c('img-score-win')}
                src={images['student-score-win20.png']}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
