import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logout } from '../../actions/auth';

const PrivateRoute = ({
  logout,
  component: Component,
  auth: { isAuthenticated, loading },
  ...rest
}) => {
  if (isAuthenticated && !localStorage.token) {
    logout();
  }
  console.log('isAuthenticated:', isAuthenticated);
  console.log('loading:', loading);
  //console.debug('PrivateRoute:', isAuthenticated);
  return (
    <Route
      {...rest}
      render={props =>
        !isAuthenticated && !loading ? (
          <Redirect to="/login" />
        ) : (
          <Component {...props} />
        )
      }
    />
  );
};

PrivateRoute.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { logout })(PrivateRoute);
