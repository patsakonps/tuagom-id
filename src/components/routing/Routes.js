import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import PrivateRoute from '../routing/PrivateRoute';
import Home from '../home/Home';
import Login from '../login/Login';
import Register from '../register/Register';
import Dashboard from '../dashboard/Dashboard';
import PlayBoard from '../play-board/PlayBoard';
import Planet from '../planet/Planet';
import State from '../state/State';
import Lesson from '../lesson/Lesson';
// import Bonus from '../bonus/Bonus';
import Test from '../test/Test';
import TestPlanet from '../test/TestPlanet';
import TestState from '../test/TestState';

import { ImagesProvider } from './contexts/ImagesContext';

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/test">
        <Test />
      </Route>
      <Route exact path="/test/state">
        <TestState />
      </Route>
      <Route exact path="/test/planet">
        <TestPlanet />
      </Route>
      <PrivateRoute exact path="/">
        <Redirect to="/home" />
      </PrivateRoute>
      <PrivateRoute exact path="/home" component={Home} />
      <Route exact path="/login" component={Login} />
      <PrivateRoute exact path="/register" component={Register} />
      <PrivateRoute exact path="/dashboard/:id" component={Dashboard} />
      <PrivateRoute exact path="/play-board/:id" component={PlayBoard} />
      <PrivateRoute exact path="/planet/:id" component={Planet} />
      <PrivateRoute exact path="/state/:id/:level" component={State} />
      <PrivateRoute exact path="/lesson/:id/:level/:state" component={Lesson} />
    </Switch>
  );
};

export default Routes;
