import React, { useState } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
// import {
//   Search as SearchIcon,
//   DoubleArrow as DoubleArrowIcon,
//   CheckCircle as CheckCircleIcon
// } from '@material-ui/icons';
import { joinClasses, extractErrorRespMsg } from 'utils/utility';
import { clickAnime } from 'utils/element';
import { login } from '../../services/auth';
import { useImagesContext } from '../../contexts/ImagesContext';
import { useDispatch, useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    display: 'flex',
    flexFlow: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100vw',
    height: '100vh',
    overflow: 'auto',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },

  'img-top-bar': {
    width: '100%',
    maxHeight: '4rem',
    zIndex: 1,
  },
  'img-left-bar': {
    position: 'fixed',
    top: 0,
    left: '3%',
    zIndex: 0,
    width: '2.5%',
    height: '100vh',
  },
  'img-right-bar': {
    position: 'fixed',
    top: 0,
    right: '3%',
    zIndex: 0,
    width: '2.5%',
    height: '100vh',
  },
  'img-bottom-bar': {
    width: '100%',
    maxHeight: '4rem',
    zIndex: 1,
  },
  'input-login': {
    fontSize: '2rem',
    fontWeight: 600,
    margin: '.5rem',
    borderColor: 'transparent',
    width: '100%',
    //lineHeight: '3rem',
    '&:focus': {
      outline: 'none',
    },
  },
  'btn-login': {
    marginTop: '1rem',
    width: '42%',
    maxWidth: '10rem',
  },
  'img-id': {
    position: 'absolute',
    width: '25%',
    maxWidth: '10rem',
    paddingTop: '.2rem',
    zIndex: 2,
  },
  'img-frame': {
    width: '80%',
    maxWidth: '20rem',
  },
  'img-name': {
    width: '75%',
    maxWidth: '34rem',
    marginTop: '1rem',
  },
  'img-match': {
    width: '50%',
    marginTop: '1rem',
  },
  'img-logout': {
    width: '36%',
    paddingLeft: '1rem',
  },
  'img-next': {
    width: '36%',
    paddingRight: '1rem',
  },
  'img-line': {
    width: '85%',
    margin: '1rem 0px',
  },
  'img-label': {
    width: '70%',
    paddingLeft: '1rem',
  },
  'img-01': { width: '90%' },
  'img-02': { width: '90%' },
  'img-feild': { width: '90%' },
  'text-desc': {
    textAlign: 'center',
    color: '#6c2912',
    textTransform: 'uppercase',
    fontSize: '2rem',
    textShadow: '.01rem .01rem .01rem #000000',
    fontWeight: 'bolder',
    '-webkit-text-stroke': '.018rem #fff',
  },
  'container-devider': {
    width: '60%',
    height: '.2rem',
    backgroundColor: '#6c2912',
  },
}));

const Login = (props) => {
  const { images } = useImagesContext();
  const c = joinClasses(useStyles({ ...props }));

  const [loginReqFlag, setLoginReqFlag] = useState(false);
  const [loginSuccessFlag, setLoginSuccessFlag] = useState(false);
  const [loginErrorFlag, setLoginErrorFlag] = useState(false);

  const handleClickLogin = () => {};

  const loginService = (user, next = null, error = null) => {
    setLoginReqFlag(true);
    setLoginSuccessFlag(false);
    setLoginErrorFlag(false);
    login(
      user,
      (resp) => {
        setLoginSuccessFlag(true);
        next && next(resp);
      },
      (err) => {
        setLoginErrorFlag(true);
        error && error(err);
      },
      () => {
        setLoginReqFlag(false);
      }
    );
  };

  const compStudentInfo1 = () => {
    return (
      <div
        style={{
          display: 'flex',
          flexFlow: 'column',
          alignItems: 'center',
        }}
      >
        <Grid container style={{ width: '100%', marginBottom: '.5rem' }}>
          <Grid item xs={6}>
            <img
              className={c('img-label')}
              src={images['student-snapshort-level.png']}
            />
          </Grid>
          <Grid item xs={2}>
            <img
              className={c('img-01')}
              src={images['student-snapshort-01.png']}
            />
          </Grid>
          <Grid item xs={4}>
            <img
              className={c('img-feild')}
              src={images['student-snapshort-feild.png']}
            />
          </Grid>
        </Grid>
        <Grid container style={{ width: '100%', marginBottom: '.5rem' }}>
          <Grid item xs={6}>
            <img
              className={c('img-label')}
              src={images['student-snapshort-score.png']}
            />
          </Grid>
          <Grid item xs={2}>
            <img
              className={c('img-01')}
              src={images['student-snapshort-01.png']}
            />
          </Grid>
          <Grid item xs={4}>
            <img
              className={c('img-feild')}
              src={images['student-snapshort-feild.png']}
            />
          </Grid>
        </Grid>
        <Grid container style={{ width: '100%', marginBottom: '.5rem' }}>
          <Grid item xs={6}>
            <img
              className={c('img-label')}
              src={images['student-snapshort-point.png']}
            />
          </Grid>
          <Grid item xs={2}>
            <img
              className={c('img-02')}
              src={images['student-snapshort-02.png']}
            />
          </Grid>
          <Grid item xs={4}>
            <img
              className={c('img-feild')}
              src={images['student-snapshort-feild.png']}
            />
          </Grid>
        </Grid>
        <Grid container style={{ width: '100%', marginBottom: '.5rem' }}>
          <Grid item xs={6}>
            <img
              className={c('img-label')}
              src={images['student-snapshort-comment.png']}
            />
          </Grid>
          <Grid item xs={2}>
            <img
              className={c('img-02')}
              src={images['student-snapshort-02.png']}
            />
          </Grid>
          <Grid item xs={4}>
            <img
              className={c('img-feild')}
              src={images['student-snapshort-feild.png']}
            />
          </Grid>
        </Grid>
      </div>
    );
  };

  const compStudentInfo2 = () => {
    return (
      <div
        style={{
          display: 'flex',
          flexFlow: 'column',
          alignItems: 'center',
        }}
      >
        <Grid container style={{ width: '100%', marginBottom: '.5rem' }}>
          <Grid item xs={8}>
            <img
              className={c('img-label')}
              src={images['student-snapshort-gameplay.png']}
            />
          </Grid>

          <Grid item xs={4}>
            <img
              className={c('img-feild')}
              src={images['student-snapshort-feild.png']}
            />
          </Grid>
        </Grid>
        <Grid container style={{ width: '100%', marginBottom: '.5rem' }}>
          <Grid item xs={8}>
            <img
              className={c('img-label')}
              src={images['student-snapshort-ploblem.png']}
            />
          </Grid>

          <Grid item xs={4}>
            <img
              className={c('img-feild')}
              src={images['student-snapshort-feild.png']}
            />
          </Grid>
        </Grid>
        <Grid container style={{ width: '100%', marginBottom: '.5rem' }}>
          <Grid item xs={6}>
            <img
              className={c('img-label')}
              src={images['student-snapshort-study.png']}
            />
          </Grid>
          <Grid item xs={2}>
            <img
              className={c('img-02')}
              src={images['student-snapshort-02.png']}
            />
          </Grid>
          <Grid item xs={4}>
            <img
              className={c('img-feild')}
              src={images['student-snapshort-feild.png']}
            />
          </Grid>
        </Grid>
        <Grid container style={{ width: '100%', marginBottom: '.5rem' }}>
          <Grid item xs={6}>
            <img
              className={c('img-label')}
              src={images['student-snapshort-teacher-comment.png']}
            />
          </Grid>
          <Grid item xs={2}>
            <img
              className={c('img-02')}
              src={images['student-snapshort-02.png']}
            />
          </Grid>
          <Grid item xs={4}>
            <img
              className={c('img-feild')}
              src={images['student-snapshort-feild.png']}
            />
          </Grid>
        </Grid>
        <Grid container style={{ width: '100%', marginBottom: '.5rem' }}>
          <Grid item xs={6}>
            <img
              className={c('img-label')}
              src={images['student-snapshort-photo.png']}
            />
          </Grid>
          <Grid item xs={2}>
            <img
              className={c('img-02')}
              src={images['student-snapshort-02.png']}
            />
          </Grid>
          <Grid item xs={4}>
            <img
              className={c('img-feild')}
              src={images['student-snapshort-feild.png']}
            />
          </Grid>
        </Grid>
      </div>
    );
  };

  return (
    <div
      className={c('root')}
      style={{
        backgroundImage: `url(${images['student-snapshort-background.jpg']})`,
      }}
    >
      <img
        className={c('img-top-bar')}
        src={images['student-snapshort-top.png']}
      />
      <img
        className={c('img-left-bar')}
        src={images['student-snapshort-side.png']}
      />
      <img className={c('img-id')} src={images['student-snapshort-id.png']} />
      <Grid
        container
        justify='center'
        alignItems='flex-start'
        alignContent='flex-start'
        style={{ marginTop: '2rem', marginBottom: '4rem' }}
      >
        <Grid item xs={8} container justify='center'>
          <div
            style={{
              display: 'flex',
              flexFlow: 'column',
              alignItems: 'center',
            }}
          >
            <img
              className={c('img-frame')}
              src={images['student-snapshort-frame.png']}
            />
            <img
              className={c('img-name')}
              src={images['student-snapshort-name.png']}
            />
            <img
              className={c('img-match')}
              src={images['student-snapshort-matching.png']}
            />
          </div>
        </Grid>
        <Grid item container justify='center'>
          <img
            className={c('img-line')}
            src={images['student-snapshort-line.png']}
          />
        </Grid>
        <Grid item xs={11} container justify='center'>
          {compStudentInfo2()}
        </Grid>
        <Grid item container justify='center'>
          <img
            className={c('img-line')}
            style={{ marginTop: '0rem' }}
            src={images['student-snapshort-line.png']}
          />
        </Grid>
        <Grid item xs={11} container justify='space-between'>
          <img
            className={c('img-logout')}
            src={images['student-snapshort-logout.png']}
            ref={(ref) => {
              if (ref) {
                clickAnime(ref);
              }
            }}
          />

          <img
            className={c('img-next')}
            src={images['student-snapshort-next.png']}
            ref={(ref) => {
              if (ref) {
                clickAnime(ref);
              }
            }}
          />
        </Grid>
      </Grid>
      <img
        className={c('img-right-bar')}
        src={images['student-snapshort-side.png']}
      />
      <img
        className={c('img-bottom-bar')}
        src={images['student-snapshort-bottom.png']}
      />
    </div>
  );
};

export default Login;
