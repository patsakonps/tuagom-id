import React, { useState } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
// import {
//   Search as SearchIcon,
//   DoubleArrow as DoubleArrowIcon,
//   CheckCircle as CheckCircleIcon
// } from '@material-ui/icons';
import { joinClasses, extractErrorRespMsg } from 'utils/utility';
import { clickAnime } from 'utils/element';
import { login } from '../../services/auth';
import { useImagesContext } from '../../contexts/ImagesContext';
import { useDispatch, useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    display: 'flex',
    flexFlow: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100vw',
    height: '100vh',
    overflow: 'auto',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },

  'img-top-bar': {
    width: '100%',
    maxHeight: '4rem',
    zIndex: 1,
  },
  'img-left-bar': {
    position: 'fixed',
    top: 0,
    left: '3%',
    zIndex: 0,
    width: '2.5%',
    height: '100vh',
  },
  'img-right-bar': {
    position: 'fixed',
    top: 0,
    right: '3%',
    zIndex: 0,
    width: '2.5%',
    height: '100vh',
  },
  'img-bottom-bar': {
    width: '100%',
    maxHeight: '4rem',
    zIndex: 1,
  },

  'img-line': {
    width: '85%',
    margin: '1rem 0px',
  },
  'setting-id-container': {
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  'setting-th-password': {
    width: '64%',
  },
  'setting-photo': {
    width: '78%',
  },
  'setting-photo-field': {
    width: '92%',
  },
  'setting-upload': {
    width: '68%',
    margin: '1rem 0px',
  },
  'setting-id': {
    width: '80%',
    margin: '1rem 0px',
  },
  'setting-comment': {
    width: '48%',
  },
  'setting-comment-field': {
    width: '90%',
    margin: '1rem 0px',
  },
  'setting-th-teacher': {
    width: '30%',
    marginRight: '1rem',
  },
  'setting-teacher-field': {
    width: '30%',
  },
  'student-setting-ver-line': {
    width: '6%',
  },
  'setting-comment-link': {
    width: '88%',
  },
  'setting-comment-linkt-field': {
    width: '90%',
    margin: '.9rem 0px',
  },
  'setting-th-desc': {
    width: '38%',
  },
  'setting-password': {
    width: '38%',
  },
  'setting-password-field': {
    width: '50%',
    height: '24%',
    margin: '.1rem 0px 1rem 0px',
  },
  'setting-save': {
    width: '38%',
  },
  'input-setting-id': {
    position: 'absolute',
    fontSize: '2rem',
    fontWeight: 600,
    margin: '.5rem',
    borderColor: 'transparent',
    backgroundColor: 'transparent',
    width: '70%',
    height: '54%',
    '&:focus': {
      outline: 'none',
    },
  },
}));

const Login = (props) => {
  const { images } = useImagesContext();
  const c = joinClasses(useStyles({ ...props }));

  const [loginReqFlag, setLoginReqFlag] = useState(false);
  const [loginSuccessFlag, setLoginSuccessFlag] = useState(false);
  const [loginErrorFlag, setLoginErrorFlag] = useState(false);

  const handleClickLogin = () => {};

  const loginService = (user, next = null, error = null) => {
    setLoginReqFlag(true);
    setLoginSuccessFlag(false);
    setLoginErrorFlag(false);
    login(
      user,
      (resp) => {
        setLoginSuccessFlag(true);
        next && next(resp);
      },
      (err) => {
        setLoginErrorFlag(true);
        error && error(err);
      },
      () => {
        setLoginReqFlag(false);
      }
    );
  };

  return (
    <div
      className={c('root')}
      style={{
        backgroundImage: `url(${images['student-setting-background.jpg']})`,
      }}
    >
      <img
        className={c('img-top-bar')}
        src={images['student-setting-top.png']}
      />
      <img
        className={c('img-left-bar')}
        src={images['student-setting-side.png']}
      />

      <Grid
        container
        justify='center'
        alignItems='flex-start'
        alignContent='flex-start'
        style={{ marginTop: '1rem', marginBottom: '4rem' }}
      >
        <Grid item xs={10} container justify='center'>
          <Grid item xs={5}>
            <div
              style={{
                display: 'flex',
                flexFlow: 'column',
                alignItems: 'center',
              }}
            >
              <img
                className={c('setting-th-password')}
                src={images['student-setting-th-password.png']}
              />

              <div className={c('setting-id-container')}>
                <img
                  className={c('setting-id')}
                  src={images['student-setting-id.png']}
                />
                <input className={c('input-setting-id')} />
              </div>
            </div>
          </Grid>
          <Grid item xs={2} container justify='center'>
            <img
              className={c('student-setting-ver-line')}
              src={images['student-setting-line01.png']}
            />
          </Grid>
          <Grid item xs={5}>
            <div
              style={{
                display: 'flex',
                flexFlow: 'column',
                alignItems: 'center',
              }}
            >
              <img
                className={c('setting-photo')}
                src={images['student-setting-photo01.png']}
              />
              <img
                className={c('setting-upload')}
                src={images['student-setting-upload.png']}
              />
              <img
                className={c('setting-photo-field')}
                src={images['student-setting-photo-field.png']}
              />
            </div>
          </Grid>
        </Grid>
        <Grid item container justify='center'>
          <img
            className={c('img-line')}
            src={images['student-setting-line02.png']}
          />
        </Grid>
        <Grid item xs={10} container justify='center'>
          <div
            style={{
              display: 'flex',
              flexFlow: 'column',
              alignItems: 'center',
            }}
          >
            <img
              className={c('setting-comment')}
              src={images['student-setting-comment.png']}
            />
            <img
              className={c('setting-comment-field')}
              src={images['student-setting-comment01.png']}
            />
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <img
                className={c('setting-th-teacher')}
                src={images['student-setting-th-teacher.png']}
              />
              <img
                className={c('setting-teacher-field')}
                src={images['student-setting-teacher-field.png']}
              />
            </div>
          </div>
        </Grid>
        <Grid item container justify='center'>
          <img
            className={c('img-line')}
            src={images['student-setting-line02.png']}
          />
        </Grid>
        <Grid item xs={10} container justify='center'>
          <div
            style={{
              display: 'flex',
              flexFlow: 'column',
              alignItems: 'center',
            }}
          >
            <img
              className={c('setting-comment-link')}
              src={images['student-setting-comment-link.png']}
            />
            <img
              className={c('setting-comment-linkt-field')}
              src={images['student-setting-comment-link01.png']}
            />
            <img
              className={c('setting-th-desc')}
              src={images['student-setting-th-desc.png']}
            />
            <img
              className={c('setting-comment-linkt-field')}
              src={images['student-setting-comment-link01.png']}
            />
          </div>
        </Grid>
        <Grid item container justify='center'>
          <img
            className={c('img-line')}
            src={images['student-setting-line02.png']}
          />
        </Grid>
        <Grid item xs={10} container justify='center'>
          <div
            style={{
              display: 'flex',
              flexFlow: 'column',
              alignItems: 'center',
            }}
          >
            <img
              className={c('setting-password')}
              src={images['student-setting-password.png']}
            />
            <img
              className={c('setting-password-field')}
              src={images['student-setting-teacher-field.png']}
            />

            <img
              className={c('setting-save')}
              src={images['student-setting-upload.png']}
              ref={(ref) => {
                if (ref) {
                  clickAnime(ref);
                }
              }}
            />
          </div>
        </Grid>
      </Grid>
      <img
        className={c('img-right-bar')}
        src={images['student-setting-side.png']}
      />
      <img
        className={c('img-bottom-bar')}
        src={images['student-setting-bottom.png']}
      />
    </div>
  );
};

export default Login;
