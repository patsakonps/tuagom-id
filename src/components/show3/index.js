import React, { useState } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
  Grid,
  IconButton,
  InputBase,
  Chip,
  CircularProgress,
  Box,
  Typography,
} from '@material-ui/core';
// import {
//   Search as SearchIcon,
//   DoubleArrow as DoubleArrowIcon,
//   CheckCircle as CheckCircleIcon
// } from '@material-ui/icons';
import { Link, Redirect } from 'react-router-dom';
import 'antd/dist/antd.css';
import { Carousel } from 'antd';

import {
  joinClasses,
  LightenDarkenColor,
  delay,
  extractErrorRespMsg,
} from 'utils/utility';

import {
  getShow1 as getShow1Service,
  getShow2 as getShow2Service,
  getStudentShowDetail as getStudentShowDetailService,
} from '../../services/show';
import { useImagesContext } from '../../contexts/ImagesContext';
import { setSelectedBranch } from '../../actions/branch';
import { handleError } from '../../services/handleResponse';
import { logout } from '../../actions/auth';
import Show1 from '../show/show1';
import Show2 from '../show2/show2';
import imgLoading from 'images/circle-loading-animation1.gif';
const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '2160px',
    height: '3840px',
    // width: '100vw',
    // height: '100vh',
    // width: 'fit-content',
    // height: '100vh',
    overflow: 'hidden',
    // backgroundRepeat: 'no-repeat',
    // backgroundPosition: 'center',
    // backgroundSize: 'contain',
  },
  //   container: {
  //     display: 'flex',
  //     justifyContent: 'center',
  //     alignItems: 'center',
  //     width: 1212,
  //     height: '100%',
  //     transform: 'translateY(120px)',
  //   },
}));

const Show = (props) => {
  // const { images } = useImagesContext();
  const c = joinClasses(useStyles({ ...props }));
  const [showData, setShowData] = useState([]);
  const [showData2, setShowData2] = useState([]);
  const [studentShowDetail, setStudentShowDetail] = useState([]);
  const [showData1ReqFlag, setShowData1ReqFlag] = useState(false);
  const [showData2ReqFlag, setShowData2ReqFlag] = useState(false);
  const [showData1SuccessFlag, setShowData1SuccessFlag] = useState(false);
  const [showData2SuccessFlag, setShowData2SuccessFlag] = useState(false);

  const [
    getStudentShowDetailReqFlag,
    setGetStudentShowDetailReqFlag,
  ] = useState(false);
  const [
    getStudentShowDetailSuccessFlag,
    setGetStudentShowDetailSuccessFlag,
  ] = useState(false);
  const [autoplaySpeed, setAutoplaySpeed] = useState(15000);
  React.useEffect(() => {
    getShowData1();
    getShowData2();
    getStudentShowDetail();
  }, []);

  const getShowData1 = () => {
    setShowData1ReqFlag(true);
    setShowData1SuccessFlag(false);
    getShow1Service(
      (resp) => {
        setShowData(resp);
        setShowData1ReqFlag(false);
        setShowData1SuccessFlag(true);
      },
      (error) => {
        handleError(error);
        setShowData1ReqFlag(false);
      }
    );
  };

  const getShowData2 = () => {
    setShowData2ReqFlag(true);
    setShowData2SuccessFlag(false);
    getShow2Service(
      (resp) => {
        setShowData2(resp);
        setShowData2ReqFlag(false);
        setShowData2SuccessFlag(true);
      },
      (error) => {
        handleError(error);
        setShowData2ReqFlag(false);
      }
    );
  };

  const getStudentShowDetail = () => {
    setGetStudentShowDetailReqFlag(true);
    setGetStudentShowDetailSuccessFlag(false);
    getStudentShowDetailService(
      (resp) => {
        setStudentShowDetail(resp || []);
        setGetStudentShowDetailReqFlag(false);
        setGetStudentShowDetailSuccessFlag(true);
      },
      (error) => {
        handleError(error);
        setGetStudentShowDetailReqFlag(false);
      }
    );
  };
  //   const [showData, setShowData] = useState([]);

  //   React.useEffect(() => {
  //     getShow1Service(
  //       (resp) => {
  //         setShowData(resp);
  //       },
  //       (error) => {
  //         handleError(error);
  //       }
  //     );
  //   }, []);

  // const get12FristStudent = (dataList) => {
  //   return (dataList || [])
  //     .reduce((accum, level) => {
  //       return [...accum, ...level.students];
  //     }, [])
  //     .slice(0, 12);
  // };

  const handleCarouselafterChange = (current) => {
    console.log('handleCarouselafterChange:', current);

    if (current === 0) {
      //reset();
    } else if (current === 2) {
      setAutoplaySpeed(5000);
    } else if (current === 2 + (studentShowDetail || []).length) {
      setAutoplaySpeed(15000);
    }
  };

  const handleCarouselbeforeChange = (current, next) => {
    console.log('handleCarouselbeforeChange:', next);
    if (next === 0) {
      reset();
    }
  };

  const reset = () => {
    setAutoplaySpeed(15000);
    setShowData([]);
    setShowData2([]);
    getShowData1();
    getShowData2();
    getStudentShowDetail();
  };
  //console.log('showData:', get12FristStudent(showData));
  return (
    <div className={c('root')}>
      {!showData1ReqFlag &&
      !showData2ReqFlag &&
      showData1SuccessFlag &&
      showData2SuccessFlag &&
      (showData || []).length &&
      (showData2 || []).length &&
      !getStudentShowDetailReqFlag &&
      getStudentShowDetailSuccessFlag ? (
        <Carousel
          autoplay
          autoplaySpeed={autoplaySpeed}
          style={{ width: '2160px', height: '3840px' }}
          afterChange={handleCarouselafterChange}
          beforeChange={handleCarouselbeforeChange}
          pauseOnHover={false}
        >
          <div key={'cwe_1'}>
            <Show1
              ckey={'cwe_1' + 1}
              mode={'multiple'}
              data={showData.slice(0, 4)}
            />
          </div>
          <div key={'cwe_2'}>
            <Show1
              ckey={'cwe_2' + 1}
              mode={'multiple'}
              data={showData.slice(4, 8)}
            />
          </div>
          {studentShowDetail.map((student, idx) => (
            <div key={'cc_' + idx}>
              <Show1 ckey={'ccss_1' + idx} mode={'single'} data={student} />
            </div>
          ))}
          {Array.from(Array(Math.ceil(showData2.length / 10)).keys()).map(
            (_, idx) => (
              <div key={'c2_' + idx}>
                <Show2
                  ckey={'s2_' + idx}
                  startNoIdx={idx * 10}
                  data={showData2.slice(idx * 10, 10 * (idx + 1))}
                />
              </div>
            )
          )}
        </Carousel>
      ) : (
        <img src={imgLoading} />
      )}
    </div>
  );
};

const mapStateToProps = ({ config, auth }) => ({
  auth,
  config,
});

const mapDispatchToProps = { setSelectedBranch, logout };

export default connect(mapStateToProps, mapDispatchToProps)(Show);
