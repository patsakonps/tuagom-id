import React, { useState } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
  Grid,
  IconButton,
  InputBase,
  Chip,
  CircularProgress,
  Box,
  Typography,
} from '@material-ui/core';
// import {
//   Search as SearchIcon,
//   DoubleArrow as DoubleArrowIcon,
//   CheckCircle as CheckCircleIcon
// } from '@material-ui/icons';
import { Link, Redirect } from 'react-router-dom';

import { escape, escapeRegExp } from 'lodash-es';
import 'antd/dist/antd.css';
import { Carousel } from 'antd';

import {
  joinClasses,
  LightenDarkenColor,
  delay,
  extractErrorRespMsg,
} from 'utils/utility';

import { useImagesContext } from '../../contexts/ImagesContext';
import { setSelectedBranch } from '../../actions/branch';
import { handleError } from '../../services/handleResponse';
import { logout } from '../../actions/auth';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '2160px',
    height: '3840px',
    // width: '100vw',
    // height: '100vh',
    // width: 'fit-content',
    // height: '100vh',
    overflow: 'hidden',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
  },
  container: {
    top: 820,
    position: 'absolute',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
    //width: 1212,
    //height: 1316,
    //transform: 'translateY(120px)',
  },
  item: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    //marginBottom: '40px',
    '& [text-number]': {
      position: 'absolute',
      textTransform: 'uppercase',
      fontSize: '8.8rem',

      textShadow: '1px 3px 3px #000000',
      //color: '#fff',
      fontWeight: 700,
      '-webkit-text-stroke': '1px #fff',
    },

    '& [avatar]': {
      position: 'absolute',
      borderRadius: '50%',
      width: 234,
      height: 234,
      zIndex: 5,
      top: 3,
    },
    '& [single-avatar]': {
      position: 'absolute',
      borderRadius: '50%',
      width: 596,
      height: 596,
    },
    '& [text-name]': {
      textTransform: 'uppercase',
      fontSize: '3.8rem',
      fontWeight: 800,
      wordBreak: 'break-all',
      textAlign: 'center',
    },
  },
  'header-label': {
    width: 300,
  },
  'header-rank': {
    width: '100%',
    height: '169px',
  },
  'header-name': {
    width: '100%',
    height: '169px',
  },
  'header-point': {
    width: '80%',
    height: '169px',
  },
  'header-level': {
    width: '100%',
    height: '169px',
  },
  'col-rank': {
    width: '100%',
  },
  'col-name': {
    width: '100%',
  },
  'col-point': {
    width: '100%',
  },
  'col-level': {
    width: '100%',
  },
  'horizon-devider': {
    width: '100%',
    height: 4,
  },
  'vertical-devider': {
    height: 'calc(100% - 203px)',
    position: 'absolute',
    bottom: 0,
    float: 'right',
    width: 4,
  },
  framePic: {
    //width: 100,
  },
  framePicSingle: {
    width: 620,
  },
}));

const Show2 = (props) => {
  const { data, startNoIdx, ckey } = props;
  const { images } = useImagesContext();
  const c = joinClasses(useStyles({ ...props }));
  //console.log('startNoIdx:', startNoIdx);

  return (
    <div
      key={ckey}
      className={c('root')}
      style={{
        backgroundImage: `url(${images['present2-background.jpg']})`,
      }}
    >
      <div className={c('container')}>
        <Grid
          container
          spacing={1}
          style={{ position: 'relative', marginTop: '10rem' }}
        >
          <Grid
            item
            style={{ width: '12%', marginBottom: '1rem' }}
            className={c('item')}
          >
            <img
              className={c('header-rank')}
              src={images['present2-rank.png']}
            />
          </Grid>

          <Grid
            item
            style={{ width: '38%', marginBottom: '1rem' }}
            className={c('item')}
          >
            <img
              className={c('header-name')}
              src={images['present2-name.png']}
            />
          </Grid>
          <Grid
            item
            style={{ width: '30%', marginBottom: '1rem' }}
            className={c('item')}
          >
            <img
              className={c('header-point')}
              src={images['present2-point.png']}
            />
          </Grid>
          <Grid
            item
            style={{ width: '20%', marginBottom: '1rem' }}
            className={c('item')}
          >
            <img
              className={c('header-level')}
              src={images['present2-level.png']}
            />
          </Grid>
          <Grid item xs={12}>
            <img
              className={c('horizon-devider')}
              src={images['present2-line.png']}
            />
          </Grid>
          {(data || []).map((student, idx) => (
            <>
              <Grid item style={{ width: '12%' }} className={c('item')}>
                <Typography text-number=''>{startNoIdx + idx + 1}</Typography>
              </Grid>

              <Grid item style={{ width: '38%' }} className={c('item')}>
                <Grid
                  container
                  style={{ flexWrap: 'nowrap', paddingLeft: '1rem' }}
                  alignItems='center'
                  spacing={2}
                >
                  <Grid item>
                    <img
                      className={c('framePic')}
                      src={images['present2-pic-frame_2.png']}
                    />
                    <img
                      avatar=''
                      src={student?.avatar || images['present2-unnamed.png']}
                    />
                  </Grid>
                  <Grid item>
                    <Typography text-name=''>{student.name}</Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item style={{ width: '30%' }} className={c('item')}>
                <div className={c('col-point')}>
                  <Grid container spacing={2} alignItems='center'>
                    <Grid
                      item
                      style={{
                        display: 'flex',
                        justifyContent: 'center',
                        width: '100%',
                      }}
                    >
                      <Typography text-name=''>{student.pointExtra}</Typography>
                    </Grid>
                  </Grid>
                </div>
              </Grid>
              <Grid item style={{ width: '20%' }} className={c('item')}>
                <div className={c('col-level')}>
                  <Grid container spacing={2} alignItems='center'>
                    <Grid
                      item
                      style={{
                        display: 'flex',
                        justifyContent: 'center',
                        width: '100%',
                      }}
                    >
                      <Typography text-name=''>{student.levelName}</Typography>
                    </Grid>
                  </Grid>
                </div>
              </Grid>
              <Grid item xs={12}>
                <div className={c('horizon-devider')}>
                  <img
                    className={c('horizon-devider')}
                    src={images['present2-line.png']}
                  />
                </div>
              </Grid>
            </>
          ))}
          <Grid
            container
            spacing={1}
            style={{ position: 'absolute', height: '100%' }}
          >
            <Grid item style={{ width: '12%', height: '100%' }}>
              <img
                className={c('vertical-devider')}
                src={images['present2-line2.png']}
              />
            </Grid>

            <Grid item style={{ width: '38%', height: '100%' }}>
              <img
                className={c('vertical-devider')}
                src={images['present2-line2.png']}
              />
            </Grid>
            <Grid item style={{ width: '30%', height: '100%' }}>
              <img
                className={c('vertical-devider')}
                src={images['present2-line2.png']}
              />
            </Grid>
            <Grid item style={{ width: '20%', height: '100%' }}>
              <img
                className={c('vertical-devider')}
                src={images['present2-line2.png']}
              />
            </Grid>
            <Grid item xs={12}></Grid>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

const mapStateToProps = ({ config, auth }) => ({
  auth,
  config,
});

const mapDispatchToProps = { setSelectedBranch, logout };

export default connect(mapStateToProps, mapDispatchToProps)(Show2);
