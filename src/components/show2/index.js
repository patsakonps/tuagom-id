import React, { useState } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
  Grid,
  IconButton,
  InputBase,
  Chip,
  CircularProgress,
  Box,
  Typography,
} from '@material-ui/core';
// import {
//   Search as SearchIcon,
//   DoubleArrow as DoubleArrowIcon,
//   CheckCircle as CheckCircleIcon
// } from '@material-ui/icons';
import { Link, Redirect } from 'react-router-dom';

import { escape, escapeRegExp } from 'lodash-es';
import 'antd/dist/antd.css';
import { Carousel } from 'antd';

import {
  joinClasses,
  LightenDarkenColor,
  delay,
  extractErrorRespMsg,
} from 'utils/utility';

import { getShow2 as getShow2Service } from '../../services/show';
import { useImagesContext } from '../../contexts/ImagesContext';
import { setSelectedBranch } from '../../actions/branch';
import { handleError } from '../../services/handleResponse';
import { logout } from '../../actions/auth';
import Show2 from './show2';
import imgLoading from 'images/circle-loading-animation1.gif';
const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '2160px',
    height: '3840px',
    // width: '100vw',
    // height: '100vh',
    // width: 'fit-content',
    // height: '100vh',
    overflow: 'hidden',
    // backgroundRepeat: 'no-repeat',
    // backgroundPosition: 'center',
    // backgroundSize: 'contain',
    '& [class~="ant-carousel"] [class~="slick-dots-bottom"]': {
      display: 'none !important',
    },
  },
  //   container: {
  //     display: 'flex',
  //     justifyContent: 'center',
  //     alignItems: 'center',
  //     width: 1212,
  //     height: '100%',
  //     transform: 'translateY(120px)',
  //   },
}));

const Show = (props) => {
  // const { images } = useImagesContext();
  const c = joinClasses(useStyles({ ...props }));
  const [showDataReqFlag, setShowDataReqFlag] = useState(false);
  const [showDataSuccessFlag, setShowDataSuccessFlag] = useState(false);
  const [showData, setShowData] = useState([]);
  const [autoplaySpeed, setAutoplaySpeed] = useState(15000);
  React.useEffect(() => {
    getShowData();
  }, []);

  const getShowData = () => {
    setShowDataReqFlag(true);
    setShowDataSuccessFlag(false);
    getShow2Service(
      (resp) => {
        setShowData(resp);
        setShowDataReqFlag(false);
        setShowDataSuccessFlag(true);
      },
      (error) => {
        handleError(error);
        setShowDataReqFlag(false);
      }
    );
  };
  //   const [showData, setShowData] = useState([]);

  //   React.useEffect(() => {
  //     getShow2Service(
  //       (resp) => {
  //         setShowData(resp);
  //       },
  //       (error) => {
  //         handleError(error);
  //       }
  //     );
  //   }, []);

  const handleCarouselafterChange = (current) => {
    // console.log('handleCarouselafterChange');
    // if (current === 2) {
    //   setAutoplaySpeed(5000);
    // }
  };

  const handleCarouselbeforeChange = (current, next) => {
    console.log('handleCarouselbeforeChange:', next);
    if (next === 0) {
      reset();
    }
  };

  const reset = () => {
    setShowData([]);
    getShowData();
  };
  //console.log('showData:', get12FristStudent(showData));
  return (
    <div className={c('root')}>
      {!showDataReqFlag && showDataSuccessFlag && (showData || []).length ? (
        <Carousel
          autoplay
          autoplaySpeed={autoplaySpeed}
          style={{ width: '2160px', height: '3840px' }}
          afterChange={handleCarouselafterChange}
          beforeChange={handleCarouselbeforeChange}
          pauseOnHover={false}
        >
          {Array.from(Array(Math.ceil(showData.length / 10)).keys()).map(
            (_, idx) => (
              <div key={'s2_' + idx}>
                <Show2
                  ckey={'ss2_' + idx}
                  startNoIdx={idx * 10}
                  data={showData.slice(idx * 10, 10 * (idx + 1))}
                />
              </div>
            )
          )}
        </Carousel>
      ) : (
        <img src={imgLoading} />
      )}
    </div>
  );
};

const mapStateToProps = ({ config, auth }) => ({
  auth,
  config,
});

const mapDispatchToProps = { setSelectedBranch, logout };

export default connect(mapStateToProps, mapDispatchToProps)(Show);
