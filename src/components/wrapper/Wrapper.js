import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'block',
    width: '2160px',
    height: '3840px',
    cursor: 'url(/images/mouse.png),auto',
  },
  container: {
    display: 'table',
    height: '100%',
    width: '100%',
  },
  verticalHelper: {
    display: 'table-cell',
    verticalAlign: 'middle',
  },
  content: {
    margin: 'auto',
    width: 'fit-content',
  },
}));

export default function Wrapper(props) {
  const classes = useStyles();

  return (
    <div className={classes.root} onContextMenu={(e) => e.preventDefault()}>
      <div className={classes.container}>
        <div className={classes.verticalHelper}>
          <div className={classes.content}>{props.children}</div>
        </div>
      </div>
    </div>
  );
}
