import React, { useState } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
// import {
//   Search as SearchIcon,
//   DoubleArrow as DoubleArrowIcon,
//   CheckCircle as CheckCircleIcon
// } from '@material-ui/icons';
import { joinClasses, extractErrorRespMsg } from 'utils/utility';
import { clickAnime } from 'utils/element';
import { login } from '../../services/auth';
import { useImagesContext } from '../../contexts/ImagesContext';
import { useDispatch, useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    display: 'flex',
    flexFlow: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100vw',
    height: '100vh',
    overflow: 'auto',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  'container-login': {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    maxWidth: '32rem',
    height: '15%',
    backgroundRepeat: 'no-repeat',
    backgroundSize: '100% 70%',
    backgroundPosition: 'center',
  },
  'img-top-bar': {
    width: '100%',
    maxHeight: '4rem',
    zIndex: 1,
  },
  'img-left-bar': {
    position: 'fixed',
    top: 0,
    left: '3%',
    zIndex: 0,
    width: '2.5%',
    height: '100vh',
  },
  'img-right-bar': {
    position: 'fixed',
    top: 0,
    right: '3%',
    zIndex: 0,
    width: '2.5%',
    height: '100vh',
  },
  'img-bottom-bar': {
    width: '100%',
    maxHeight: '4rem',
    zIndex: 1,
  },
  'input-login': {
    fontSize: '2rem',
    fontWeight: 600,
    margin: '.5rem',
    borderColor: 'transparent',
    width: '100%',
    //lineHeight: '3rem',
    '&:focus': {
      outline: 'none',
    },
  },
  'btn-login': {
    marginTop: '1rem',
    width: '42%',
    maxWidth: '10rem',
  },
  'img-logo': {
    width: '80%',
    maxWidth: '20rem',
  },
  'text-desc': {
    textAlign: 'center',
    color: '#6c2912',
    textTransform: 'uppercase',
    fontSize: '2rem',
    textShadow: '.01rem .01rem .01rem #000000',
    fontWeight: 'bolder',
    '-webkit-text-stroke': '.018rem #fff',
  },
  'container-devider': {
    width: '60%',
    height: '.2rem',
    backgroundColor: '#6c2912',
  },
}));

const Login = (props) => {
  const { images } = useImagesContext();
  const c = joinClasses(useStyles({ ...props }));

  const [loginReqFlag, setLoginReqFlag] = useState(false);
  const [loginSuccessFlag, setLoginSuccessFlag] = useState(false);
  const [loginErrorFlag, setLoginErrorFlag] = useState(false);

  const handleClickLogin = () => {};

  const loginService = (user, next = null, error = null) => {
    setLoginReqFlag(true);
    setLoginSuccessFlag(false);
    setLoginErrorFlag(false);
    login(
      user,
      (resp) => {
        setLoginSuccessFlag(true);
        next && next(resp);
      },
      (err) => {
        setLoginErrorFlag(true);
        error && error(err);
      },
      () => {
        setLoginReqFlag(false);
      }
    );
  };

  return (
    <div
      className={c('root')}
      root=''
      style={{
        backgroundImage: `url(${images['login-background.jpg']})`,
      }}
    >
      <img className={c('img-top-bar')} src={images['login-top.png']} />
      <img className={c('img-left-bar')} src={images['login-side.png']} />

      <Grid
        container
        justify='center'
        alignItems='center'
        style={{ marginTop: '2rem', marginBottom: '4rem' }}
      >
        <Grid item xs={8} container justify='center'>
          <img className={c('img-logo')} src={images['login-logo.png']} />
        </Grid>
        <Grid
          item
          xs={8}
          className={c('container-login')}
          style={{
            backgroundImage: `url(${images['login-field.png']})`,
          }}
        >
          <input className={c('input-login')} />
        </Grid>
        <Grid item xs={8} container justify='center'>
          <Typography className={c('text-desc')}>กรอกรหัสประจำตัว</Typography>
          <Typography className={c('text-desc')}>เพื่อเข้าระบบ</Typography>
        </Grid>
        <Grid item xs={8} container justify='center'>
          <div className={c('container-devider')} />
        </Grid>
        <Grid item xs={8} container justify='center'>
          <Typography className={c('text-desc')}>
            Enter your personal id to login
          </Typography>
        </Grid>
        <Grid item xs={8} container justify='center'>
          <img
            className={c('btn-login')}
            src={images['login-ok.png']}
            ref={(ref) => {
              if (ref) {
                clickAnime(ref);
              }
            }}
            onClick={handleClickLogin}
          />
        </Grid>
      </Grid>
      <img className={c('img-right-bar')} src={images['login-side.png']} />
      <img className={c('img-bottom-bar')} src={images['login-bottom.png']} />
    </div>
  );
};

export default Login;
