import { STATE_DATA_SAVE } from './types';

export const saveStateData = token => async dispatch => {
  dispatch({
    type: STATE_DATA_SAVE,
    payload: token
  });
};
