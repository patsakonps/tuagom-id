import axios from '../host';
import * as type from './types';

export const setSelectedBranch = (val) => async (dispatch) => {
  return dispatch({
    type: type.SET_SELECTED_BRANCH,
    payload: { value: val },
  });
};
