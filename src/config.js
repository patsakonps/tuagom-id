const prod = {
  // hostUrl: 'https://study.tuagom.com/webservice',
  hostUrl: 'https://study-test.tuagom.com/study-test/service',
};

const dev = {
  // hostUrl: 'http://localhost:5000',
  hostUrl: 'http://tuagom.com:5002',
};

const config = process.env.NODE_ENV !== 'production' ? dev : prod;
// console.debug('process.env.NODE_ENV', process.env.NODE_ENV);

export default {
  // Add common config values here
  ...config,
};
