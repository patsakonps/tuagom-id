import axios from '../host';

export function getShow1(_next = null, _catch = null) {
  axios
    .get('/api/v2/student-show/1')
    .then((resp) => {
      // console.log('resp::', resp.data);
      _next && _next(resp.data);
      return;
    })
    .catch((error) => {
      _catch && _catch(error);
    });
}

export function getShow2(_next = null, _catch = null) {
  axios
    .get('/api/v2/student-show/2')
    .then((resp) => {
      // console.log('resp::', resp.data);
      _next && _next(resp.data);
      return;
    })
    .catch((error) => {
      _catch && _catch(error);
    });
}

export function getStudentShowDetail(_next = null, _catch = null) {
  axios
    .get('/api/v2/student-show/detail')
    .then((resp) => {
      // console.log('resp::', resp.data);
      _next && _next(resp.data);
      return;
    })
    .catch((error) => {
      _catch && _catch(error);
    });
}
