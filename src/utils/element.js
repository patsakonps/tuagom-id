export function clickAnime(element) {
  element.addEventListener('click', () => {
    element.style.cssText =
      'transition: all .085s ease;transform: scale(1.05);filter: drop-shadow(0px 4px 2px grey)';
    setTimeout(() => {
      element.style.cssText =
        'transition: all .085s ease;transform: scale(1);filter: none';
    }, 85);
  });
}
