import axios from 'axios';
import config from './config';

const hostUrl = config.hostUrl;

export default axios.create({
  baseURL: hostUrl
});
