import React, { Suspense } from 'react';
// import {
//   BrowserRouter as Router,
//   Switch,
//   Route,
//   Redirect,
// } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import history from 'lib/history';
import PrivateRoute from './components/routing/PrivateRoute';
import Wrapper from './components/wrapper/Wrapper';
import Login from './components/login';
import Student from './components/student-snapshort';
import StudentSetting from './components/student-setting';
import StudentScore from './components/student-score';
import { ImagesProvider } from './contexts/ImagesContext';
import { Grid, Typography } from '@material-ui/core';
import store from './store';
import imgLoading from 'images/circle-loading-animation1.gif';
import './App.css';

import setAuthToken from './utils/setAuthToken';

if (localStorage.token) {
  //localStorage.removeItem('token');
  // console.debug(`localStorage.token:${localStorage.token}`);
  setAuthToken(localStorage.token);
}

const Loading = () => (
  <Grid>
    <img src={imgLoading} />
  </Grid>
);

const LoginComponent = (props) => (
  <ImagesProvider
    intro={false}
    r={require.context('./components/login/', true, /\.(png|jpe?g|svg|gif)$/)}
  >
    <Login {...props} />
  </ImagesProvider>
);

const StudentComponent = (props) => (
  <ImagesProvider
    intro={false}
    r={require.context(
      './components/student-snapshort/',
      true,
      /\.(png|jpe?g|svg|gif)$/
    )}
  >
    <Student {...props} />
  </ImagesProvider>
);

const StudentSettingComponent = (props) => (
  <ImagesProvider
    intro={false}
    r={require.context(
      './components/student-setting/',
      true,
      /\.(png|jpe?g|svg|gif)$/
    )}
  >
    <StudentSetting {...props} />
  </ImagesProvider>
);

const StudentScoreComponent = (props) => (
  <ImagesProvider
    intro={false}
    r={require.context(
      './components/student-score/',
      true,
      /\.(png|jpe?g|svg|gif)$/
    )}
  >
    <StudentScore {...props} />
  </ImagesProvider>
);

function App() {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Suspense fallback={<Loading />}>
          <Switch>
            <Route exact path='/login' component={LoginComponent} />
            <Route exact path='/student' component={StudentComponent} />
            <Route
              exact
              path='/student-setting'
              component={StudentSettingComponent}
            />
            <Route
              exact
              path='/student-score'
              component={StudentScoreComponent}
            />
          </Switch>
        </Suspense>
      </Router>
    </Provider>
  );
}

export default App;
